﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    public class Plant : GameObject
    {
        public Plant()
        {

        }

        public Plant(int attack, int defence, int currentHP, int maxHP) :base(attack, defence, currentHP, maxHP)
        {
        }

        public override bool IsEaten { get { return IsDead; } }
        public override void performAttack(GameObject target)
        {
            //nothing - plants can't attack
        }

        public override void TakeTurn(Map map, Position currentPosition)
        {
            //plants may spread in future
        }
    }
}
