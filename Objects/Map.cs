﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    public  class Map
    {
        /*
         * X,Y ----->
         * |
         * |
         * |
         * |
         * 
         */

        public int Turn { get; private set; }
        private GameObject[,] playingField;
        private List<GameObject> gameObjectList;
        private Dictionary<GameObject, Position> gameObjectPositions;
        private Random random = new Random();

        public int Height { get { return playingField.GetLength(0); } }
        public int Width { get { return playingField.GetLength(1); } }

        public Map(int height, int width)
        {
            playingField = new GameObject[height, width];
            gameObjectList = new List<GameObject>();
            gameObjectPositions = new Dictionary<GameObject, Position>();
        }

        public bool Contains(Type type)
        {
            foreach (GameObject gameobject in gameObjectList)
            {
                if (gameobject.GetType() == type)
                {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsAtLeastTwo(Type type)
        {
            int counter = 0;
            foreach (GameObject gameobject in gameObjectList)
            {
                if (gameobject.GetType() == type)
                {
                    counter++;
                    if (counter == 2)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public int Count(Type type)
        {
            int counter = 0;
            foreach (GameObject gameobject in gameObjectList)
            {
                if (gameobject.GetType() == type)
                {
                    counter++;
                }
            }
            return counter;
        }

        public void MoveGameObject(GameObject gameObject, Position position)
        {
            if (position == Position.Invalid)
            {
                throw new ArgumentException("position is invalid");
            }

            this[gameObjectPositions[gameObject]] = null;
            this[position] = gameObject;

            gameObjectPositions[gameObject] = position;
        }

        //both positions are inclusive
        public List<Position> getEmptyPositions(Position topLeft, Position bottomRight)
        {
            List<Position> positions = new List<Position>();

            for (int x = topLeft.X; x <= bottomRight.X; x++)
            {
                for (int y = topLeft.Y; y <= bottomRight.Y; y++)
                {
                    if (playingField[x,y] == null)
                    {
                        positions.Add(new Position(x,y));
                    }
                }
            }

            return positions;
        }

        //returns whether Gameobject has been spawned
        public bool SpawnGameObject(Direction direction, GameObject gameObject)
        {
            List<Position> emptyPositions;

            switch (direction)
            {
                case Direction.Left:
                    emptyPositions = getEmptyPositions(new Position(0, 0), new Position(playingField.GetLength(0) -1, 0));
                    break;
                case Direction.Right:
                    emptyPositions = getEmptyPositions(new Position(0, playingField.GetLength(1) -1), new Position(playingField.GetLength(0) -1, playingField.GetLength(1) -1));
                    break;
                case Direction.Top:
                    emptyPositions = getEmptyPositions(new Position(0, 0), new Position(0, playingField.GetLength(1) -1));
                    break;
                case Direction.Bottom:
                    emptyPositions = getEmptyPositions(new Position(playingField.GetLength(0) -1, 0), new Position(playingField.GetLength(0) -1, playingField.GetLength(1) -1));
                    break;
                default:
                    throw new NotImplementedException();
            }

            if (emptyPositions.Count == 0)
            {
                return false;
            }

            SpawnGameObject(emptyPositions[random.Next(0, emptyPositions.Count)], gameObject);
            return true;
        }

        //returns whether Gameobject has been spawned
        public bool SpawnGameObject(int x, int y, GameObject gameObject)
        {
            return SpawnGameObject(new Position(x,y), gameObject);
        }

        public bool SpawnGameObject()
        {
            GameObject gameObject;

            switch (random.Next(3))
            {
                case 0:
                    gameObject = new Plant();
                    break;
                case 1:
                    gameObject = new Herbivore();
                    break;
                case 2:
                    gameObject = new Carnivore();
                    break;
                default:
                    throw new NotImplementedException();
            }

            gameObject.Attack = random.Next(0,5);
            gameObject.Defense = random.Next(0,5);
            gameObject.CurrentHP = random.Next(1,5);
            gameObject.MaxHP = random.Next(1,5);

            if (gameObject.CurrentHP > gameObject.MaxHP)
            {
                gameObject.MaxHP = gameObject.CurrentHP;
            }

            return SpawnGameObject(gameObject);
        }

        public bool SpawnGameObject(Type type)
        {
            GameObject gameObject = (GameObject)Activator.CreateInstance(type);

            gameObject.Attack = random.Next(0, 5);
            gameObject.Defense = random.Next(0, 5);
            gameObject.CurrentHP = random.Next(1, 5);
            gameObject.MaxHP = random.Next(1, 5);

            if (gameObject.CurrentHP > gameObject.MaxHP)
            {
                gameObject.MaxHP = gameObject.CurrentHP;
            }

            return SpawnGameObject(gameObject);
        }

        public bool SpawnGameObject(GameObject gameObject)
        {
            var positions = getEmptyPositions(new Position(0, 0), new Position(this.Height -1, this.Width -1));

            if (positions.Count == 0)
            {
                return false;
            }

            SpawnGameObject(positions[random.Next(positions.Count)], gameObject);

            return true;
        }

        public bool SpawnGameObject(Position position, GameObject gameObject)
        {
            if (this[position] == null)
            {
                gameObjectList.Add(gameObject);
                gameObjectPositions.Add(gameObject, position);
                this[position] = gameObject;
                return true;
            }
            return false;
        }

        public GameObject this[int x, int y]
        {
            get
            {
                if (x < 0 || y < 0 || x >= playingField.GetLength(0) || y >= playingField.GetLength(1))
                {
                    return null;
                }

                return playingField[x,y];
            }
        }

        public GameObject this[Position position]
        {
            get { return this[position.X, position.Y]; }
            private set { playingField[position.X, position.Y] = value; }
        }

        public List<GameObject> SpawnedGameObjects { get { return gameObjectList; } }

        public void PlayOneRound()
        {
            foreach (GameObject currentGameObject in gameObjectList)
            {
                currentGameObject.TakeTurn(this, gameObjectPositions[currentGameObject]);
            }

            //Delete eaten GameObjects
            List<GameObject> toBeRemoved = new List<GameObject>();
            foreach (KeyValuePair<GameObject, Position> keyValuePair in gameObjectPositions)
            {
                if (keyValuePair.Key.IsEaten)
                {
                    this[keyValuePair.Value] = null;
                    toBeRemoved.Add(keyValuePair.Key);
                }
            }

            foreach (GameObject gameObject in toBeRemoved)
            {
                gameObjectList.Remove(gameObject);
                gameObjectPositions.Remove(gameObject);
            }
            Turn++;
        }
    }

    public enum Direction
    {
        Top, Bottom, Left, Right
    }

    public struct Position
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Position Above { get { return new Position(X - 1, Y); } }
        public Position Below { get { return new Position(X + 1, Y); } }
        public Position ToRight { get { return new Position(X, Y + 1); } }
        public Position ToLeft { get { return new Position(X, Y - 1); } }

        public readonly static Position Invalid =  new Position(-1, -1);


        public static bool operator ==(Position position1, Position position2)
        {
            return position1.X == position2.X && position1.Y == position2.Y;
        }

        public static bool operator !=(Position position1, Position position2)
        {
            return !(position1 == position2);
        }

        public override bool Equals(object obj)
        {
            return obj is Position position &&
                   X == position.X &&
                   Y == position.Y;
        }

        public override int GetHashCode()
        {
            int hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

        public override string ToString()
        {
            return $"{X},{Y}";
        }
    }
}
