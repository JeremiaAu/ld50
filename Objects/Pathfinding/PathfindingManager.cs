﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects.Pathfinding
{
	//Tutorial for A* Pathfinding using C#: https://dotnetcoretutorials.com/2020/07/25/a-search-pathfinding-algorithm-in-c/
	public static class PathfindingManager
	{

		public static List<Position> FindPath(Map map, Position startPosition, Position finishPosition)
        {
            Tile startTile = new Tile(startPosition, finishPosition);

			List<Tile> activeTiles = new List<Tile>();
            List<Tile> visitedTiles = new List<Tile>();

            activeTiles.Add(startTile);

			while (activeTiles.Any())
			{
				activeTiles = activeTiles.OrderBy(t => t.CostDistance).ToList();
				Tile checkTile = activeTiles.First();
				if (checkTile.Position == finishPosition)
				{
					//We are at the destination!
					//We can actually loop through the parents of each tile. 
					List<Position> list = new List<Position>();
					Tile currentTile = checkTile.Parent;

					while (currentTile.Position != startPosition)
                    {
						list.Add(currentTile.Position);
						currentTile = currentTile.Parent;
					}
					list.Reverse();
					return list;
				}
				activeTiles.RemoveAt(0);
				visitedTiles.Add(checkTile);
				List<Tile> walkableTiles = GetWalkableTiles(map, checkTile, finishPosition);
				foreach (Tile walkableTile in walkableTiles)
				{
					//We have already visited this tile so we don't need to do so again!
					if (visitedTiles.Any(t => t.Position == walkableTile.Position))
						continue;
					//It's already in the active list, but that's OK, maybe this new tile has a better value (e.g. We might zigzag earlier but this is now straighter). 
					if (activeTiles.Any(t => t.Position == walkableTile.Position))
					{
						var existingTile = activeTiles.First(t => t.Position==walkableTile.Position);
						if (existingTile.CostDistance > checkTile.CostDistance)
						{
							activeTiles.Remove(existingTile);
							activeTiles.Add(walkableTile);
						}
					}
					else
					{
						//We've never seen this tile before so add it to the list. 
						activeTiles.Add(walkableTile);
					}
				}
			}
			//No Path Found!
			return null;
		}

		private static List<Tile> GetWalkableTiles(Map map, Tile currentTile, Position target)
		{
			var possibleTiles = new List<Tile>()
			{
				new Tile(currentTile.Position.Above,target,currentTile),
				new Tile(currentTile.Position.Below,target,currentTile),
				new Tile(currentTile.Position.ToRight,target,currentTile),
				new Tile(currentTile.Position.ToLeft,target,currentTile),
			};

			return possibleTiles
					.Where(tile => tile.Position.X >= 0 && tile.Position.X < map.Height)
					.Where(tile => tile.Position.Y >= 0 && tile.Position.Y < map.Width)
					.Where(tile => map[tile.Position] == null || tile.Position == target)
					.ToList();
		}

        public static Position FindNextStepToFood(Map map, Position startingPosition, Type foodType)
        {
			Tile startTile = new Tile(startingPosition);

			Queue<Tile> activeTiles = new Queue<Tile>();
			List<Tile> visitedTiles = new List<Tile>();

			activeTiles.Enqueue(startTile);

			while (activeTiles.Any())
			{
				Tile checkTile = activeTiles.Dequeue();
				if (startTile != checkTile && map[checkTile.Position] != null && foodType == map[checkTile.Position].GetType())
				{
					//We are at the destination!
					Tile currentTile = checkTile.Parent;

					while (currentTile.Parent.Position != startingPosition)
					{
						currentTile = currentTile.Parent;
					}
					return currentTile.Position;
				}
				visitedTiles.Add(checkTile);
				List<Tile> walkableTiles = GetWalkableTiles(map, checkTile, foodType);
				foreach (Tile walkableTile in walkableTiles)
				{
					//We have already visited this tile so we don't need to do so again!
					if (visitedTiles.Any(t => t.Position == walkableTile.Position))
						continue;

					//We've never seen this tile before so add it to the list. 
					activeTiles.Enqueue(walkableTile);
				}
			}
			//No Path Found!
			return Position.Invalid;
		}

        private static List<Tile> GetWalkableTiles(Map map, Tile currentTile, Type foodType)
        {
			var possibleTiles = new List<Tile>()
			{
				new Tile(currentTile.Position.Above,currentTile),
				new Tile(currentTile.Position.Below,currentTile),
				new Tile(currentTile.Position.ToRight,currentTile),
				new Tile(currentTile.Position.ToLeft,currentTile),
			};

			return possibleTiles
					.Where(tile => tile.Position.X >= 0 && tile.Position.X < map.Height)
					.Where(tile => tile.Position.Y >= 0 && tile.Position.Y < map.Width)
					.Where(tile => map[tile.Position] == null || map[tile.Position].GetType() == foodType)
					.ToList();
		}
    }
}
