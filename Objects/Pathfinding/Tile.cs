﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects.Pathfinding
{
    public class Tile
    {
		public Tile(Position position, Position target, Tile parent = null)
		{ 
			Position = position;
			Parent = parent;

			SetDistance(target);

			if (parent != null)
            {
				Cost = parent.Cost + 1;
            }
			else
            {
				Cost = 0;
            }
		}

		public Tile(Position position, Tile parent = null)
		{
			Position = position;
			Parent = parent;

			if (parent != null)
			{
				Cost = parent.Cost + 1;
			}
			else
			{
				Cost = 0;
			}
		}

		public Position Position { get; set; }
        public int Cost { get; set; }
		public int Distance { get; set; }
		public int CostDistance => Cost + Distance;
		public Tile Parent { get; set; }

		//The distance is essentially the estimated distance, ignoring walls to our target. 
		//So how many tiles left and right, up and down, ignoring walls, to get there. 
		public void SetDistance(Position target)
		{
			this.Distance = Math.Abs(target.X - Position.X) + Math.Abs(target.Y - Position.Y);
		}
	}
}
