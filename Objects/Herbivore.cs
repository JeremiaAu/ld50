﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    public class Herbivore : GameObject
    {
        public Herbivore()
        {
        }

        public Herbivore(int attack, int defence, int currentHP, int maxHP) : base(attack, defence, currentHP, maxHP)
        {

        }

        public override void performAttack(GameObject target)
        {
            int damageInflicted = target.receiveDamage(Attack);

            if (target is Plant)
            {
                CurrentHP += damageInflicted;
                if (CurrentHP > MaxHP)
                {
                    CurrentHP = MaxHP;
                }
            }
        }

        public override void TakeTurn(Map map, Position currentPosition)
        {
            SeekAndEatFood(map, currentPosition, typeof(Plant));
        }
    }
}
