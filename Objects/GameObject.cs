﻿using Objects.Pathfinding;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    public abstract class GameObject
    {
        private string name;
        private int attack;
        private int defense;
        private int currentHP;
        private int maxHP;
        private bool isEaten = false;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        protected GameObject()
        {

        }

        protected GameObject(int attack, int defence, int currentHP, int maxHP)
        {
            Attack = attack;
            Defense = defence;
            CurrentHP = currentHP;
            MaxHP = maxHP;
        }

        public string Name { get => name; set { name = value; OnPropertyChanged(); } }
        public int Attack { get => attack; set { attack = value; OnPropertyChanged(); } }
        public int Defense { get => defense; set { defense = value; OnPropertyChanged(); } }
        public int CurrentHP
        {
            get => currentHP;
            set
            {
                currentHP = value;
                OnPropertyChanged();
                OnPropertyChanged("IsDead");
            }
        }
        public int MaxHP { get => maxHP; set { maxHP = value; OnPropertyChanged(); } }
        public bool IsDead { get { return CurrentHP <= 0; } }
        public virtual bool IsEaten { get => isEaten; set => isEaten = value; }
        //public int CurrentActions { get; set; }
        //public int MaxActions { get; set; } //aka Speed

        public abstract void performAttack(GameObject target);
        public abstract void TakeTurn(Map map, Position currentPosition);

        //returns damageTaken
        public int receiveDamage(int damage)
        {
            int damageAfterDefense = damage - Defense;
            if (damageAfterDefense > 0)
            {
                CurrentHP -= damageAfterDefense;
                return damageAfterDefense;
            }
            return 0;
        }

        //returns whether it was successfull
        protected bool SeekAndEatFood(Map map, Position position,Type foodType)
        {
            if (map.Contains(foodType))
            {
                if (performAttackIfFood(map, position.Above, foodType));
                else if (performAttackIfFood(map, position.Below, foodType)) ;
                else if (performAttackIfFood(map, position.ToRight, foodType)) ;
                else if (performAttackIfFood(map, position.ToLeft, foodType)) ;
                else
                {
                    //go to Food
                    Position nextStep = PathfindingManager.FindNextStepToFood(map, position, foodType);

                    if (nextStep == Position.Invalid)
                    {
                        //something exists, but it is inaccessible
                        return false;
                    }

                    map.MoveGameObject(this, nextStep);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool performAttackIfFood(Map map, Position position, Type foodType)
        {
            if (map[position] != null && map[position].GetType() == foodType)
            {
                performAttack(map[position]);
                return true;
            }
            return false;
        }
    }
}
