﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    public class Carnivore : GameObject
    {
        public Carnivore()
        {
        }

        public Carnivore(int attack, int defence, int currentHP, int maxHP) : base(attack, defence, currentHP, maxHP)
        {

        }

        public override void performAttack(GameObject target)
        {
            if (target.GetType() == typeof(Herbivore) || target.GetType() == typeof(Carnivore))
            {
                target.receiveDamage(Attack);

                if (target.IsDead)
                {
                    CurrentHP = target.MaxHP;

                    if (CurrentHP > MaxHP)
                    {
                        CurrentHP = MaxHP;
                    }

                    target.IsEaten = true;
                }
            }
            else
            {
                throw new Exception("Carnivores only eat meat");
            }
            
        }

        public override void TakeTurn(Map map, Position currentPosition)
        {
            if(!SeekAndEatFood(map, currentPosition, typeof(Herbivore)))
            {
                if (map.ContainsAtLeastTwo(typeof(Carnivore)))
                {
                    SeekAndEatFood(map, currentPosition, typeof(Carnivore));
                }
            }
        }
    }
}
