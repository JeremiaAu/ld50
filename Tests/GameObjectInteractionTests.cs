﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Objects;
using System;

namespace Tests
{
    [TestClass]
    public class GameObjectInteractionTests
    {
        [TestMethod]
        public void CarnivoreEatsHerbivore()
        {
            Carnivore attacker = new Carnivore(attack: 2, defence: 1, currentHP: 1, maxHP: 4);
            Herbivore victim = new Herbivore(2, 1, 2, 4);

            attacker.performAttack(victim);

            Assert.AreEqual(attacker.MaxHP, 4);
            Assert.AreEqual(attacker.CurrentHP, 1);

            Assert.AreEqual(victim.CurrentHP, 1);
            Assert.IsFalse(victim.IsDead);
            Assert.IsFalse(victim.IsEaten);

            attacker.performAttack(victim);


            Assert.AreEqual(attacker.MaxHP, 4);
            Assert.AreEqual(attacker.CurrentHP, attacker.MaxHP);

            Assert.AreEqual(victim.CurrentHP, 0);
            Assert.IsTrue(victim.IsDead);
            Assert.IsTrue(victim.IsEaten);
        }

        [TestMethod]
        public void HerbivoreEatsPlant()
        {
            Herbivore attacker = new Herbivore(attack: 2, defence: 1, currentHP: 1, maxHP: 4);
            Plant victim = new Plant(2, 1, 2, 4);

            attacker.performAttack(victim);

            Assert.AreEqual(attacker.MaxHP, 4);
            Assert.AreEqual(attacker.CurrentHP, 2);

            Assert.AreEqual(victim.CurrentHP, 1);
            Assert.IsFalse(victim.IsDead);
            Assert.IsFalse(victim.IsEaten);

            attacker.performAttack(victim);


            Assert.AreEqual(attacker.MaxHP, 4);
            Assert.AreEqual(attacker.CurrentHP, 3);

            Assert.AreEqual(victim.CurrentHP, 0);
            Assert.IsTrue(victim.IsDead);
            Assert.IsTrue(victim.IsEaten);
        }

        [TestMethod]
        public void PlantEatsCarnivore()
        {
            Plant attacker = new Plant(attack: 2, defence: 1, currentHP: 1, maxHP: 4);
            Carnivore victim = new Carnivore(2, 1, 2, 4);

            Assert.AreEqual(victim.Attack, 2);
            Assert.AreEqual(victim.Defense, 1);
            Assert.AreEqual(victim.CurrentHP, 2);
            Assert.AreEqual(victim.MaxHP, 4);
        }
    }
}
