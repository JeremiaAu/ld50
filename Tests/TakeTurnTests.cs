﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Objects;
using System;

namespace Tests
{
    [TestClass]
    public class TakeTurnTests
    {
        [TestMethod]
        public void EatTest()
        {
            Map map = new Map(5, 10);

            Plant grass = new Plant(0, 0, 2, 2);
            Herbivore rabbit = new Herbivore(1, 0, 2, 2);
            Carnivore wolf = new Carnivore(1, 0, 2, 2);


            map.SpawnGameObject(0, 0, grass);
            map.SpawnGameObject(1, 0, rabbit);
            map.SpawnGameObject(1, 1, wolf);

            Assert.AreEqual(2, grass.CurrentHP);
            Assert.AreEqual(2, rabbit.CurrentHP);
            Assert.AreEqual(2, wolf.CurrentHP);

            map.PlayOneRound();

            Assert.AreEqual(1, grass.CurrentHP);
            Assert.AreEqual(1, rabbit.CurrentHP);
            Assert.AreEqual(2, wolf.CurrentHP);

            map.PlayOneRound();

            Assert.AreEqual(0, grass.CurrentHP);
            Assert.AreEqual(1, rabbit.CurrentHP);
            Assert.AreEqual(2, wolf.CurrentHP);
            Assert.IsTrue(grass.IsDead);
            Assert.IsTrue(grass.IsEaten);

            map.PlayOneRound();

            Assert.AreEqual(0, grass.CurrentHP);
            Assert.AreEqual(0, rabbit.CurrentHP);
            Assert.AreEqual(2, wolf.CurrentHP);
            Assert.IsTrue(grass.IsDead);
            Assert.IsTrue(grass.IsEaten);
            Assert.IsTrue(rabbit.IsDead);
            Assert.IsTrue(rabbit.IsEaten);
        }

        [TestMethod]
        public void MoveAndEatTest()
        {
            Map map = new Map(5, 10);

            Plant grass = new Plant(0, 0, 2, 2);
            Herbivore rabbit = new Herbivore(1, 0, 2, 2);
            Carnivore wolf = new Carnivore(1, 0, 2, 2);


            map.SpawnGameObject(0, 0, grass);
            map.SpawnGameObject(2, 0, rabbit);
            map.SpawnGameObject(2, 2, wolf);

            //start
            Assert.AreEqual(2, grass.CurrentHP);
            Assert.AreEqual(2, rabbit.CurrentHP);
            Assert.AreEqual(2, wolf.CurrentHP);

            map.PlayOneRound();

            //both went one step
            Assert.AreEqual(2, grass.CurrentHP);
            Assert.AreEqual(2, rabbit.CurrentHP);
            Assert.AreEqual(2, wolf.CurrentHP);

            map.PlayOneRound();

            //rabbit ate, wolf went one step
            Assert.AreEqual(1, grass.CurrentHP);
            Assert.AreEqual(2, rabbit.CurrentHP);
            Assert.AreEqual(2, wolf.CurrentHP);


            map.PlayOneRound();

            //rabbit ate, wolf attacked
            Assert.AreEqual(0, grass.CurrentHP);
            Assert.AreEqual(1, rabbit.CurrentHP);
            Assert.AreEqual(2, wolf.CurrentHP);
            Assert.IsTrue(grass.IsDead);
            Assert.IsTrue(grass.IsEaten);

            map.PlayOneRound();

            //rabbit did nothing, wolf attacked
            Assert.AreEqual(0, grass.CurrentHP);
            Assert.AreEqual(0, rabbit.CurrentHP);
            Assert.AreEqual(2, wolf.CurrentHP);
            Assert.IsTrue(grass.IsDead);
            Assert.IsTrue(grass.IsEaten);
            Assert.IsTrue(rabbit.IsDead);
            Assert.IsTrue(rabbit.IsEaten);

        }
    }
}
