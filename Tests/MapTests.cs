﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Objects;
using System;
using System.Collections.Generic;

namespace Tests
{
    [TestClass]
    public class MapTests
    {
        [TestMethod]
        public void SpawnOnTop()
        {
            Map map = new Map(10, 10);
            Plant plant = new Plant(1, 1, 1, 1);
            Plant roses = new Plant(1, 1, 1, 1);


            map.SpawnGameObject(1, 1, plant);

            Assert.AreEqual(map[1, 1], plant);


            Assert.IsFalse(map.SpawnGameObject(1, 1, roses));

            Assert.AreEqual(map[1, 1], plant);
            Assert.AreEqual(1, map.SpawnedGameObjects.Count);
        }

        [TestMethod]
        public void SpawnMultipleGameObjects()
        {
            Map map = new Map(10, 10);
            Plant plant = new Plant(1, 1, 1, 1);
            Plant rose = new Plant(1, 1, 1, 1);
            Plant bush = new Plant(1, 1, 1, 1);
            Plant tree = new Plant(1, 1, 1, 1);


            map.SpawnGameObject(1, 1, plant);
            map.SpawnGameObject(1, 2, rose);
            map.SpawnGameObject(1, 3, bush);
            map.SpawnGameObject(2, 1, tree);

            Assert.AreEqual(map[1, 1], plant);

            Assert.AreEqual(4, map.SpawnedGameObjects.Count);
        }

        [TestMethod]
        public void GetEmptyPositionsAllEmpty()
        {
            Map map = new Map(10, 10);

            List<Position> positions = map.getEmptyPositions(new Position(0, 0), new Position(9, 9));

            Assert.AreEqual(100, positions.Count);
        }

        [TestMethod]
        public void GetEmptyPositionsSomeEmpty()
        {
            Map map = new Map(10, 10);

            map.SpawnGameObject(1, 1, new Carnivore(1, 2, 3, 4));
            map.SpawnGameObject(1, 2, new Carnivore(1, 2, 3, 4));
            map.SpawnGameObject(1, 3, new Carnivore(1, 2, 3, 4));

            Assert.AreEqual(97, map.getEmptyPositions(new Position(0, 0), new Position(9, 9)).Count);
            Assert.AreEqual(0, map.getEmptyPositions(new Position(1, 1), new Position(1, 3)).Count);
            Assert.AreEqual(3, map.getEmptyPositions(new Position(0, 0), new Position(1, 1)).Count);
            Assert.AreEqual(5, map.getEmptyPositions(new Position(0, 0), new Position(2, 1)).Count);
            Assert.AreEqual(13, map.getEmptyPositions(new Position(0, 0), new Position(3, 3)).Count);
        }


        [TestMethod]
        public void SpawnTooMuchTop()
        {
            Map map = new Map(10, 10);

            for (int i = 0; i < 10; i++)
            {
                map.SpawnGameObject(Direction.Top, new Herbivore(1, 2, 3, 4));
            }

            Assert.IsFalse(map.SpawnGameObject(Direction.Top, new Herbivore(1, 2, 3, 4)));

            Assert.AreEqual(10, map.SpawnedGameObjects.Count);
        }

        [TestMethod]
        public void SpawnFullTopRow()
        {
            Map map = new Map(10, 10);

            for (int i = 0; i < 10; i++)
            {
                map.SpawnGameObject(Direction.Top, new Herbivore(1, 2, 3, 4));
            }

            Assert.AreEqual(10, map.SpawnedGameObjects.Count);
            for (int y = 0; y < 10; y++)
            {
                Assert.IsNotNull(map[0, y]);
            }
        }

        [TestMethod]
        public void SpawnFullBottomRow()
        {
            Map map = new Map(10, 10);

            for (int i = 0; i < 10; i++)
            {
                map.SpawnGameObject(Direction.Bottom, new Herbivore(1, 2, 3, 4));
            }

            Assert.AreEqual(10, map.SpawnedGameObjects.Count);
            for (int y = 0; y < 10; y++)
            {
                Assert.IsNotNull(map[9, y]);
            }
        }

        [TestMethod]
        public void SpawnFullRightColumn()
        {
            Map map = new Map(10, 10);

            for (int i = 0; i < 10; i++)
            {
                map.SpawnGameObject(Direction.Right, new Herbivore(1, 2, 3, 4));
            }

            Assert.AreEqual(10, map.SpawnedGameObjects.Count);
            for (int x = 0; x < 10; x++)
            {
                Assert.IsNotNull(map[x, 9]);
            }
        }

        [TestMethod]
        public void SpawnFullLeftColumn()
        {
            Map map = new Map(10, 10);

            for (int i = 0; i < 10; i++)
            {
                map.SpawnGameObject(Direction.Left, new Herbivore(1, 2, 3, 4));
            }

            Assert.AreEqual(10, map.SpawnedGameObjects.Count);
            for (int x = 0; x < 10; x++)
            {
                Assert.IsNotNull(map[x, 0]);
            }
        }

        [TestMethod]
        public void GetInvalidPositionTest()
        {
            Map map = new Map(10, 10);

            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    map.SpawnGameObject(x, y, new Plant(1,2,3,4));
                }
            }

            Assert.IsNotNull(map[0, 0]);

            Assert.IsNull(map[-1, -1]);
            Assert.IsNull(map[20, 0]);
            Assert.IsNull(map[0, 11]);
            Assert.IsNull(map[10, 0]);
            Assert.IsNull(map[-1, 0]);
            Assert.IsNull(map[-1, -1]);
            Assert.IsNull(map[-1, -1]);
            Assert.IsNull(map[-99, 99]);
            Assert.IsNull(map[99, -99]);
        }

        [TestMethod]
        public void UnevenMapTest()
        {
            Map map = new Map(10, 5);

            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 5; y++)
                {
                    map.SpawnGameObject(x, y, new Plant(1, 2, 3, 4));
                }
            }

            Assert.IsNull(map[10, 5]);
            Assert.IsNull(map[-1, 0]);
            Assert.IsNull(map[0, -1]);

            Assert.IsNotNull(map[9, 4]);


        }
    }
}
