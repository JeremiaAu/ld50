﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Objects;
using Objects.Pathfinding;
using System;
using System.Collections.Generic;

namespace Tests
{
    [TestClass]
    public class PathfindingTests
    {
        [TestMethod]
        public void FindTarget()
        {
            Map map = new Map(10, 5);

            Carnivore wolf = new Carnivore(1, 2, 3, 4);
            Herbivore target = new Herbivore(1, 2, 3, 4); //Rabbit
            Position wolfPosition = new Position(0, 0);
            Position targetPosition = new Position(1, 2);

            map.SpawnGameObject(wolfPosition, wolf);
            map.SpawnGameObject(targetPosition, target);

            List<Position> steps = PathfindingManager.FindPath(map, wolfPosition, targetPosition);

            Assert.AreEqual(2, steps.Count);
        }

        [TestMethod]
        public void FindTargetAroundObstacle()
        {
            Map map = new Map(10, 5);

            Carnivore wolf = new Carnivore(1, 2, 3, 4);
            Herbivore target = new Herbivore(1, 2, 3, 4); //Rabbit
            Position wolfPosition = new Position(0, 0);
            Position targetPosition = new Position(2, 0);

            Plant obstacle1 = new Plant(1, 2, 3, 4);
            Plant obstacle2 = new Plant(1, 2, 3, 4);
            Plant obstacle3 = new Plant(1, 2, 3, 4);
            Plant obstacle4 = new Plant(1, 2, 3, 4);

            map.SpawnGameObject(1, 0, obstacle1);
            map.SpawnGameObject(1, 1, obstacle2);
            map.SpawnGameObject(1, 2, obstacle3);
            map.SpawnGameObject(1, 3, obstacle4);

            map.SpawnGameObject(wolfPosition, wolf);
            map.SpawnGameObject(targetPosition, target);


            List<Position> steps = PathfindingManager.FindPath(map, wolfPosition, targetPosition);

            Assert.AreEqual(9, steps.Count);
        }

        [TestMethod]
        public void FindNextStepToNearestFood()
        {
            Map map = new Map(10, 5);

            Herbivore rabbit = new Herbivore(1, 2, 3, 4);
            Plant food1 = new Plant(0, 0, 3, 4);
            Plant food2 = new Plant(0, 0, 3, 4);
            Plant food3 = new Plant(0, 0, 3, 4);
            Plant food4 = new Plant(0, 0, 3, 4);

            Position rabbitPosition = new Position(0, 0);

            map.SpawnGameObject(0, 4, food1);
            map.SpawnGameObject(1, 3, food2);
            map.SpawnGameObject(2, 4, food3);
            map.SpawnGameObject(3, 0, food4);

            map.SpawnGameObject(rabbitPosition, rabbit);


            Position nextStep = PathfindingManager.FindNextStepToFood(map, rabbitPosition, typeof(Plant));
            Assert.AreEqual(new Position(1, 0), nextStep);

            map.MoveGameObject(rabbit, nextStep);
            nextStep = PathfindingManager.FindNextStepToFood(map, nextStep, typeof(Plant));
            Assert.AreEqual(new Position(2, 0), nextStep);
        }

        [TestMethod]
        public void FindNextStepToNearestFoodObstacle()
        {
            Map map = new Map(10, 5);

            Herbivore rabbit = new Herbivore(1, 2, 3, 4);
            Carnivore obstacle = new Carnivore(1, 2, 3, 4);
            Plant food1 = new Plant(0, 0, 3, 4);
            Plant food2 = new Plant(0, 0, 3, 4);
            Plant food3 = new Plant(0, 0, 3, 4);
            Plant food4 = new Plant(0, 0, 3, 4);

            Position rabbitPosition = new Position(0, 0);


            map.SpawnGameObject(0, 4, food1);
            map.SpawnGameObject(1, 3, food2);
            map.SpawnGameObject(2, 4, food3);
            map.SpawnGameObject(3, 0, food4);

            map.SpawnGameObject(rabbitPosition, rabbit);
            map.SpawnGameObject(1, 0, obstacle);


            Position nextStep = PathfindingManager.FindNextStepToFood(map, rabbitPosition, typeof(Plant));
            Assert.AreEqual(new Position(0, 1), nextStep);

            map.MoveGameObject(rabbit, nextStep);
            nextStep = PathfindingManager.FindNextStepToFood(map, nextStep, typeof(Plant));
            Assert.IsTrue(nextStep == new Position(1, 1) || nextStep == new Position(0, 2));

            map.MoveGameObject(rabbit, nextStep);
            nextStep = PathfindingManager.FindNextStepToFood(map, nextStep, typeof(Plant));
            Assert.IsTrue(nextStep == new Position(1, 2) || nextStep == new Position(0, 3));
        }

        [TestMethod]
        public void FindNextStepToNearestFoodNothingFound()
        {
            Map map = new Map(10, 5);

            Herbivore rabbit = new Herbivore(1, 2, 3, 4);
            Carnivore obstacle = new Carnivore(1, 2, 3, 4);
            Position rabbitPosition = new Position(0, 0);

            map.SpawnGameObject(rabbitPosition, rabbit);
            map.SpawnGameObject(1, 0, obstacle);

            Position nextStep = PathfindingManager.FindNextStepToFood(map, rabbitPosition, typeof(Plant));
            Assert.AreEqual(Position.Invalid, nextStep);
        }

        [TestMethod]
        public void FindNextStepToNearestFoodCarnivore2Carnivore()
        {
            Map map = new Map(10, 5);

            Carnivore attacker = new Carnivore(1, 2, 3, 4);
            Carnivore victim = new Carnivore(1, 2, 3, 4);

            Position attackerPosition = new Position(0, 0);
            Position victimPosition = new Position(3, 0);

            map.SpawnGameObject(attackerPosition, attacker);
            map.SpawnGameObject(victimPosition, victim);


            Position nextStep = PathfindingManager.FindNextStepToFood(map, attackerPosition, typeof(Carnivore));
            Assert.AreEqual(new Position(1, 0), nextStep);

            map.MoveGameObject(attacker, nextStep);
            nextStep = PathfindingManager.FindNextStepToFood(map, nextStep, typeof(Carnivore));
            Assert.AreEqual(new Position(2, 0), nextStep);
        }
    }
}
