﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LD50
{
    /// <summary>
    /// Interaction logic for GameOverWindow.xaml
    /// </summary>
    public partial class GameOverWindow : Window
    {
        private MainWindow callerWindow;
        public GameOverWindow()
        {
            InitializeComponent();
        }


        public void Initialize(int turns, MainWindow callerWindow )
        {
            this.callerWindow = callerWindow;
            TbInfoText.Text = TbInfoText.Text.Replace("X", turns.ToString());
        }
        bool pressedButton = false;
        private void BtnPlayAgain_Click(object sender, RoutedEventArgs e)
        {
            callerWindow.GameOverBack(false);
            pressedButton = true;
            this.Close();
        }

        private void BtnKeepPlaying_Click(object sender, RoutedEventArgs e)
        {
            callerWindow.GameOverBack(true);
            pressedButton = true;
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (pressedButton == false)
            {
                callerWindow.GameOverBack(false);
            }
        }
    }
}
