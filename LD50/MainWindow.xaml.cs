﻿using Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LD50
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Rectangle[,] backgrounds;

        private Map map = new Map(8, 16);
        public MainWindow()
        {
            InitializeComponent();

            InitializeMap();
            DrawMapFloor();
            InitializeGame();
        }

        public void InitializeGame()
        {
            for (int i = 0; i < 10; i++)
            {
                map.SpawnGameObject();
            }

            DrawMapContents();

            updateTurnCounter();
            SetDataContext(null);

            UpdateAnimalBar();
        }

        private bool keepPlaying = false;
        internal void GameOverBack(bool keepPlaying)
        {
            this.keepPlaying = keepPlaying;

            if (keepPlaying)
            {
                this.keepPlaying = true;
            }
            else
            {
                RemoveMapContents();
                InitializeGame();
            }
        }

        private void updateTurnCounter()
        {
            LblTurn.Content = "Turn " + map.Turn;
        }

        private void RemoveMapContents()
        {
            for (int i = 0; i < GridPlayingField.Children.Count; i++)
            {
                if (GridPlayingField.Children[i].Uid != "Background")
                {
                    GridPlayingField.Children.RemoveAt(i);
                    i--;
                }
            }
        }

        private void DrawMapFloor()
        {
            backgrounds = new Rectangle[map.Height, map.Width];

            Rectangle currentBackground;
            for (int x = 0; x < map.Height; x++)
            {
                for (int y = 0; y < map.Width; y++)
                {
                    if ((x+y) % 2 == 0)
                    {
                        currentBackground = new Rectangle() { Fill = Brushes.Wheat, Uid = "Background" };
                    }
                    else
                    {
                        currentBackground = new Rectangle() { Fill = Brushes.Tan, Uid = "Background" };
                    }

                    backgrounds[x, y] = currentBackground;

                    currentBackground.MouseLeftButtonUp += Shape_MouseLeftButtonUp;
                    currentBackground.MouseLeftButtonDown += Shape_MouseLeftButtonDown;

                    Grid.SetRow(currentBackground, x);
                    Grid.SetColumn(currentBackground, y);
                    GridPlayingField.Children.Add(currentBackground);
                }
            }
        }

        private Brush plantBrush = Brushes.ForestGreen;
        private Brush herbivoreBrush = Brushes.SlateBlue;
        private Brush carnivoreBrush = Brushes.Firebrick;
        private PointCollection carnivorePolygonPoints;
        private void DrawMapContents()
        {
            if (carnivorePolygonPoints == null)
            {
                carnivorePolygonPoints = new PointCollection(3);
                carnivorePolygonPoints.Add(new Point(0,2));
                carnivorePolygonPoints.Add(new Point(1,0));
                carnivorePolygonPoints.Add(new Point(2,2));
            }

            GameObject currentGameObject;
            Shape currentShape;

            for (int x = 0; x < map.Height; x++)
            {
                for (int y = 0; y < map.Width; y++)
                {
                    currentGameObject = map[x, y];
                    if (currentGameObject != null)
                    {
                        if (currentGameObject is Plant)
                        {
                            currentShape = new Rectangle() {Fill = plantBrush };
                        }
                        else if(currentGameObject is Herbivore)
                        {
                            currentShape = new Ellipse() { Fill = herbivoreBrush };

                        }
                        else if (currentGameObject is Carnivore)
                        {
                            currentShape = new Polygon() { Fill = carnivoreBrush, Points=carnivorePolygonPoints, Stretch=Stretch.Uniform };
                        }
                        else
                        {
                            throw new NotImplementedException();
                        }
                        currentShape.Margin = new Thickness(2);
                        currentShape.MouseLeftButtonUp += Shape_MouseLeftButtonUp;
                        currentShape.MouseLeftButtonDown += Shape_MouseLeftButtonDown;

                        Grid.SetRow(currentShape, x);
                        Grid.SetColumn(currentShape, y);
                        GridPlayingField.Children.Add(currentShape);
                    }
                }
            }
        }

        private Shape selectedShape;
        private Rectangle currentBackground;
        private GameObject selectedGameObject = null;
        private void Shape_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectedShape = sender as Shape;
        }

        private void Shape_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (sender == selectedShape)
            {
                //select clicked GameObject
                int x = Grid.GetRow(selectedShape);
                int y = Grid.GetColumn(selectedShape);
                selectedGameObject = map[x,y];

                SetDataContext(selectedGameObject);


                if (currentBackground != null)
                {
                    currentBackground.StrokeThickness = 0;
                }

                currentBackground = backgrounds[x, y];
                currentBackground.Stroke = Brushes.DarkRed;
                currentBackground.StrokeThickness = 2;

            }
        }

        private void SetDataContext(GameObject gameObject)
        {
            TbAttack.DataContext = gameObject;
            TbDefense.DataContext = gameObject;
            TbCurrentHP.DataContext = gameObject;
            TbMaxHP.DataContext = gameObject;
            TblGameObjectInfo.DataContext = gameObject;

            if (gameObject == null)
            {
                TblGameObjectName.Foreground = Brushes.Black;
                TblGameObjectName.Text = "Nothing";
            }
            else
            {
                TblGameObjectName.Text = gameObject.GetType().Name;
                switch (gameObject)
                {
                    case Plant p:
                        TblGameObjectName.Foreground = plantBrush;
                        break;
                    case Herbivore h:
                        TblGameObjectName.Foreground = herbivoreBrush;
                        break;
                    case Carnivore c:
                        TblGameObjectName.Foreground = carnivoreBrush;
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        private void InitializeMap()
        {
            GridPlayingField.RowDefinitions.Clear();
            GridPlayingField.ColumnDefinitions.Clear();

            for (int x = 0; x < map.Height; x++)
            {
                RowDefinition rowDefinition = new RowDefinition();
                //rowDefinition.Height = new GridLength(1, GridUnitType.Star);
                rowDefinition.SharedSizeGroup = "A";
                //rowDefinition.MinHeight = 10;

                GridPlayingField.RowDefinitions.Add(rowDefinition);
            }

            for (int y = 0; y < map.Width; y++)
            {
                ColumnDefinition columnDefinition = new ColumnDefinition();
                //columnDefinition.Width = new GridLength(1, GridUnitType.Star);
                //columnDefinition.MinWidth = 10;
                columnDefinition.SharedSizeGroup = "A";


                GridPlayingField.ColumnDefinitions.Add(columnDefinition);
            }
        }

        private void NextTurn_Click(object sender, RoutedEventArgs e)
        {
            map.PlayOneRound();

            if (selectedGameObject != null)
            {
                if (selectedGameObject.IsEaten)
                {
                    selectedGameObject = null;
                    SetDataContext(null);
                }
                else
                {
                    SetDataContext(null);
                    SetDataContext(selectedGameObject);
                }
            }


            if (map.Turn % 2 == 0)
            {
                int herbivoreCount = map.Count(typeof(Herbivore));

                for (int i = 0; i < herbivoreCount; i++)
                {
                    map.SpawnGameObject(typeof(Herbivore));
                }

                UpdateAnimalBar();

                if (LbAnimals.Maximum / 2 <= LbAnimals.Value)
                {
                    //looks like animals will win!!!
                    for (int i = 0; i < 3; i++)
                    {
                        map.SpawnGameObject(typeof(Herbivore));
                    }
                }
                else
                {
                    //looks like plant will win!!!
                    for (int i = 0; i < 3; i++)
                    {
                        map.SpawnGameObject(typeof(Plant));
                    }
                }
            }

            updateTurnCounter();

            RemoveMapContents();
            DrawMapContents();

            UpdateAnimalBar();
        }

        private void UpdateAnimalBar()
        {
            LbAnimals.Value = map.Count(typeof(Herbivore)) + map.Count(typeof(Carnivore));

            if ((LbAnimals.Value >= LbAnimals.Maximum || LbAnimals.Value < 2) && !keepPlaying)
            {
                var gameoverWindow = new GameOverWindow();
                gameoverWindow.Initialize(map.Turn, this);
                gameoverWindow.ShowDialog();
            }
        }

        //Set MaxHP to currentHP if currentHP is bigger
        private void TbCurrentHP_TextChanged(object sender, TextChangedEventArgs e)
        {
            int currentHP, maxHP;

            if (int.TryParse(TbCurrentHP.Text, out currentHP))
            {
                if (int.TryParse(TbMaxHP.Text, out maxHP))
                {
                    if (currentHP > maxHP)
                    {
                        TbMaxHP.Text = TbCurrentHP.Text; 
                    }
                }
            }
        }

        private void TbMaxHP_TextChanged(object sender, TextChangedEventArgs e)
        {
            int currentHP, maxHP;

            if (int.TryParse(TbCurrentHP.Text, out currentHP))
            {
                if (int.TryParse(TbMaxHP.Text, out maxHP))
                {
                    if (currentHP > maxHP)
                    {
                        TbCurrentHP.Text = TbMaxHP.Text;
                    }
                }
            }
        }

        //Make the playing field as tall as wide
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double actualWidth = 0;
            for (int i = Grid.GetColumn(GridPlayingField); i < Grid.GetColumn(GridPlayingField) + Grid.GetColumnSpan(GridPlayingField); i++)
            {
                if (i >= MainGrid.ColumnDefinitions.Count)
                    break;

                actualWidth += MainGrid.ColumnDefinitions[i].ActualWidth;
            }

            double actualHeight = 0;
            for (int i = Grid.GetRow(GridPlayingField); i < Grid.GetRow(GridPlayingField) + Grid.GetRowSpan(GridPlayingField); i++)
            {
                if (i >= MainGrid.RowDefinitions.Count)
                    break;
                actualHeight += MainGrid.RowDefinitions[i].ActualHeight;
            }

            double actualHeightPerRow = actualHeight / GridPlayingField.RowDefinitions.Count;
            double actualWidthPerColumn = actualWidth / GridPlayingField.ColumnDefinitions.Count;

            if (actualHeightPerRow > actualWidthPerColumn)
            {
                GridPlayingField.Height = actualWidthPerColumn * GridPlayingField.RowDefinitions.Count;
                GridPlayingField.Width = actualWidthPerColumn * GridPlayingField.ColumnDefinitions.Count;
            }
            else
            {
                GridPlayingField.Height = actualHeightPerRow * GridPlayingField.RowDefinitions.Count;
                GridPlayingField.Width = actualHeightPerRow * GridPlayingField.ColumnDefinitions.Count;
            }
        }
    }
}
